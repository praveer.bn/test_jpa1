


package com.Servlets;

import com.dao.DaoImpl;
import com.entity.Doctor;
import com.entity.Patient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/sp")
public class ShowP extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        DaoImpl dao=new DaoImpl();

        List<Patient> list=dao.showp();

        printWriter.print("<table border=2px>");
        printWriter.print("<th>ID</th><th> NAME</th><th>DISEASE</th><th>AGE</th><th>FEEDBACK</th>");

        for (Patient s:list) {
            printWriter.println("<tr><td>"+s.getPid()+"</td><td>"+s.getpName()+"</td><td>"+s.getDisease()+"</td><td>"+s.getAge()+"</td><td>"+s.getFeedback()+"</td></tr>");
        }
        printWriter.print("<br><a href='index.jsp'>menu</a>");

    }
}
