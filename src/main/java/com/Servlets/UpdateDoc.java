package com.Servlets;

import com.dao.DaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/ud")
public class UpdateDoc extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        int fb=Integer.parseInt(req.getParameter("fee"));
        int id=Integer.parseInt(req.getParameter("id"));
        DaoImpl dao=new DaoImpl();
        dao.uopdateFee(id,fb);
        resp.sendRedirect("index.jsp");
    }
}
