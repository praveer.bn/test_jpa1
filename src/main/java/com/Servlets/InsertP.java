package com.Servlets;

import com.dao.DaoImpl;
import com.entity.Doctor;
import com.entity.Patient;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/ip")
public class InsertP extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String name=req.getParameter("name");
        String fb=req.getParameter("fb");
        int age=Integer.parseInt(req.getParameter("age"));
        String disease=req.getParameter("ds");
        int id=Integer.parseInt(req.getParameter("id"));
        Patient patient=new Patient();
        patient.setFeedback(fb);
        patient.setDisease(disease);
        patient.setAge(age);
        patient.setpName(name);
        DaoImpl dao=new DaoImpl();
        dao.insertPatient(patient,id);

        resp.sendRedirect("index.jsp");
    }
}
