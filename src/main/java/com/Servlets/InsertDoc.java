package com.Servlets;

import com.dao.DaoImpl;
import com.entity.Doctor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/id")
public class InsertDoc extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String name=req.getParameter("name");
        String spl=req.getParameter("spl");
        int fee=Integer.parseInt(req.getParameter("fee"));
        int exp=Integer.parseInt(req.getParameter("exp"));
        Doctor doctor=new Doctor();
        doctor.setdName(name);
        doctor.setFee(fee);
        doctor.setExp(exp);
        doctor.setSpl(spl);
        DaoImpl dao=new DaoImpl();
        dao.insertDoctor(doctor);
        resp.sendRedirect("index.jsp");
    }
}
