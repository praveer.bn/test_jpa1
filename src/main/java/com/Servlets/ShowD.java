package com.Servlets;

import com.dao.DaoImpl;
import com.entity.Doctor;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/sd")
public class ShowD extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        DaoImpl dao=new DaoImpl();

        List<Doctor> list=dao.showD();

        printWriter.print("<table border=2px>");
        printWriter.print("<th>ID</th><th> NAME</th><th>FEE</th><th>specialization</th><th>experience</th><th>update</th><th>delete</th>");

        for (Doctor s:list) {
            printWriter.println("<tr><td>"+s.getDid()+"</td><td>"+s.getdName()+"</td><td>"+s.getFee()+"</td><td>"+s.getSpl()+"</td><td>"+s.getExp()+"</td><td><a href=''>update</a></td><td><a href=''>delete</a></td></tr>");
        }
        printWriter.print("<br><a href='index.jsp'>menu</a>");

    }
}
