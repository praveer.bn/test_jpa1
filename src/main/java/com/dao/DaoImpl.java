package com.dao;

import com.entity.Doctor;
import com.entity.Patient;
import com.factory.FactoryProvider;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DaoImpl implements Dao{
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    public void insertPatient(Patient patient,int id) throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
//        Patient patient=new Patient();
//        System.out.println("enter the patient name");
//        patient.setpName(bufferedReader.readLine());
//        System.out.println("enter the patient age");
//        patient.setAge(Integer.parseInt(bufferedReader.readLine()));
//        System.out.println("enter the patient feedback");
//        patient.setFeedback(bufferedReader.readLine());
//        System.out.println("enter the patient disease");
//        patient.setDisease(bufferedReader.readLine());
//        entityManager.persist(patient);
//        System.out.println("assign the doctor,enter the id of doctor");
//        int id =Integer.parseInt(bufferedReader.readLine());
        Doctor doctor= entityManager.find(Doctor.class,id);
        doctor.addP(patient);
        patient.addD(doctor);
        entityManager.getTransaction().commit();
       entityManager.close();

    }

    public void insertDoctor(Doctor doctor) throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
//        Doctor doctor=new Doctor();
//        System.out.println("enter the doctor name");
//        doctor.setdName(bufferedReader.readLine());
//        System.out.println("enter the doctor exp");
//        doctor.setExp(Integer.parseInt(bufferedReader.readLine()));
//        System.out.println("enter the doctor specilization");
//        doctor.setSpl(bufferedReader.readLine());
//        System.out.println("enter the fee");
//        doctor.setFee(Integer.parseInt(bufferedReader.readLine()));
        entityManager.persist(doctor);
        entityManager.getTransaction().commit();
        entityManager.close();
    }


    public void updateFeedback(int id,String feedback) throws IOException {
        EntityManager entityManager=FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
//        System.out.println("enter the id of patient");
//        int id =Integer.parseInt(bufferedReader.readLine());
//        System.out.println("enter the updated feedback");
//        String feedback=bufferedReader.readLine();
        Patient patient =entityManager.find(Patient.class,id);
        patient.setFeedback(feedback);
        entityManager.persist(patient);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void uopdateFee(int id,int fee) throws IOException {
        EntityManager entityManager=FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
//        System.out.println("enter the id of doctor");
//        int id =Integer.parseInt(bufferedReader.readLine());
//        System.out.println("enter the updated fee");
//        int fee =Integer.parseInt(bufferedReader.readLine());
        Doctor doctor =entityManager.find(Doctor.class,id);
        doctor.setFee(fee);
        entityManager.persist(doctor);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public List<Doctor> showD() {
        EntityManager entityManager=FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();

        CriteriaQuery<Doctor> criteriaQuery=criteriaBuilder.createQuery(Doctor.class);
        Root<Doctor> from=criteriaQuery.from(Doctor.class);
        CriteriaQuery<Doctor> select=criteriaQuery.select(from);
        TypedQuery<Doctor> typedQuery=entityManager.createQuery(select);
        List<Doctor> list=typedQuery.getResultList();
        list.stream().forEach(System.out::println);
        return list;

    }

    @Override
    public List<Patient> showp() {
        EntityManager entityManager=FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Patient> criteriaQuery=criteriaBuilder.createQuery(Patient.class);
        Root<Patient> from=criteriaQuery.from(Patient.class);
        CriteriaQuery<Patient> select=criteriaQuery.select(from);
        TypedQuery<Patient> typedQuery=entityManager.createQuery(select);
        List<Patient> list=typedQuery.getResultList();
        list.stream().forEach(System.out::println);
        return list;
    }

    public void deletep(int id) throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
//        System.out.println("enter the id of patient");
//        int id =Integer.parseInt(bufferedReader.readLine());
        Patient patient= entityManager.find(Patient.class,id);
        entityManager.remove(patient);
        entityManager.getTransaction().commit();
        entityManager.close();

    }

    @Override
    public void deleted(int id) throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
//        System.out.println("enter the id of doctor");
//        int id =Integer.parseInt(bufferedReader.readLine());
        Doctor doctor= entityManager.find(Doctor.class,id);
        doctor.setPatientList(null);
        entityManager.remove(doctor);
        entityManager.getTransaction().commit();
        entityManager.close();

    }

    @Override
    public void fetchBYSpelization() throws IOException {
        System.out.println("enter the spelization");
        String spl=bufferedReader.readLine();
        EntityManager entityManager=FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery=criteriaBuilder.createQuery();
        Root<Doctor> from=criteriaQuery.from(Doctor.class);
        CriteriaQuery<Object> select=criteriaQuery.select(from);
        select.where(criteriaBuilder.equal(from.get("spl"),spl));
        TypedQuery<Object> typedQuery=entityManager.createQuery(select);
        List<Object> list=typedQuery.getResultList();
        list.stream().forEach(System.out::println);

    }

    @Override
    public void fetchBYrating() throws IOException {
        System.out.println("enter the feedback");
        String spl=bufferedReader.readLine();
        EntityManager entityManager=FactoryProvider.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery=criteriaBuilder.createQuery();
        Root<Patient> from=criteriaQuery.from(Patient.class);
        CriteriaQuery<Object> select=criteriaQuery.select(from);
        select.where(criteriaBuilder.equal(from.get("feedback"),spl));
        TypedQuery<Object> typedQuery=entityManager.createQuery(select);
        List<Object> list=typedQuery.getResultList();
        list.stream().forEach(System.out::println);
    }

    @Override
    public void fetchPatientBasisOFDocNAme() throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        System.out.println("enter the name of doctor");
        String name=bufferedReader.readLine();
        Query query=entityManager.createQuery("Select d from Doctor d where d.dName= '"+name+"'");
        List<Doctor> li=query.getResultList();
        li.stream().forEach(System.out::println);
    }

    @Override
    public void showPatientForPerticularDoctor() throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        System.out.println("enter the id of doctor");
        int id=Integer.parseInt(bufferedReader.readLine());
        Doctor doctor=entityManager.find(Doctor.class,id);
        System.out.println(doctor.getPatientList());
        entityManager.close();
    }

    @Override
    public void showDoctorForPerticularpatient() throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        System.out.println("enter the id of patient");
        int id=Integer.parseInt(bufferedReader.readLine());
        Patient patient=entityManager.find(Patient.class,id);
        System.out.println(patient.getDoctorList());
        entityManager.close();
    }

    @Override
    public void assigningDoctoPatient() throws IOException {
        EntityManager entityManager= FactoryProvider.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery=criteriaBuilder.createQuery();
        Root<Doctor> from=criteriaQuery.from(Doctor.class);
        CriteriaQuery<Object> select=criteriaQuery.select(from);
        TypedQuery<Object> typedQuery=entityManager.createQuery(select);
        List<Object> list=typedQuery.getResultList();
        list.stream().forEach(System.out::println);
        System.out.println("*****************************************************************************");
        System.out.println("enter the patient id");
        int pid =Integer.parseInt(bufferedReader.readLine());
        Patient patient=entityManager.find(Patient.class,pid);
        System.out.println("assign the doctor,enter the id of doctor");
        int id =Integer.parseInt(bufferedReader.readLine());
        Doctor doctor= entityManager.find(Doctor.class,id);
        List list1=new ArrayList<>();
        List list2=new ArrayList<>();
        list1.add(doctor);
        list2.add(patient);
        patient.setDoctorList(list1);
        doctor.setPatientList(list2);
        entityManager.persist(patient);
        entityManager.persist(doctor);
        System.out.println("done");

    }
}
