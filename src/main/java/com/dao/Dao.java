package com.dao;

import com.entity.Doctor;
import com.entity.Patient;

import java.io.IOException;
import java.util.List;

public interface Dao {
    void insertPatient(Patient patient,int id) throws IOException;
    void insertDoctor(Doctor doctor) throws IOException;

    void updateFeedback(int id,String feedback) throws IOException;
    void uopdateFee(int id,int fee) throws IOException;
    List<Doctor> showD();
    List<Patient> showp();
    void deletep(int id) throws IOException;
    void deleted(int id) throws IOException;
    void fetchBYSpelization() throws IOException;
    void fetchBYrating() throws IOException;
    void fetchPatientBasisOFDocNAme() throws IOException;
    void showPatientForPerticularDoctor() throws IOException;
    void showDoctorForPerticularpatient() throws IOException;
    void assigningDoctoPatient() throws IOException;


}
