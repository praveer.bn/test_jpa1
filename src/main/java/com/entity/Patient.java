package com.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int pid;
    private String pName;
    private  int age;
    private String disease;
    private String feedback;
    @ManyToMany(mappedBy = "patientList",cascade =CascadeType.ALL)
    private List<Doctor> doctorList;

    public Patient() {
    }

    public Patient(String pName, int age, String disease, String feedback, List<Doctor> doctorList) {
        this.pName = pName;
        this.age = age;
        this.disease = disease;
        this.feedback = feedback;
        this.doctorList = doctorList;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public List<Doctor> getDoctorList() {
        return doctorList;
    }

    public void setDoctorList(List<Doctor> doctorList) {
        this.doctorList = doctorList;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "pid=" + pid +
                ", pName='" + pName + '\'' +
                ", age=" + age +
                ", disease='" + disease + '\'' +
                ", feedback='" + feedback + '\'' +
                ", doctorList=" + doctorList +
                '}';
    }

    public  void addD(Doctor doctor){

            doctorList=new ArrayList<Doctor>();

        doctorList.add(doctor);

    }
}
