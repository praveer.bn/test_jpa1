package com.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Doctor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int did;
    private String dName;
    private int exp;
    private String spl;
    private int fee;
    @ManyToMany(cascade =CascadeType.ALL)
    private List<Patient> patientList;

    public Doctor() {
    }

    public Doctor(String dName, int exp, String spl, int fee, List<Patient> patientList) {
        this.dName = dName;
        this.exp = exp;
        this.spl = spl;
        this.fee = fee;
        this.patientList = patientList;
    }

    public int getDid() {
        return did;
    }

    public void setDid(int did) {
        this.did = did;
    }

    public String getdName() {
        return dName;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public String getSpl() {
        return spl;
    }

    public void setSpl(String spl) {
        this.spl = spl;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }

    public List<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "did=" + did +
                ", dName='" + dName + '\'' +
                ", exp=" + exp +
                ", spl='" + spl + '\'' +
                ", fee=" + fee +
                '}';
    }

    public  void addP(Patient patient){

            patientList=new ArrayList<Patient>();

        patientList.add(patient);
    }
}
